import 'dart:developer';
//import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AddSesion extends StatefulWidget {
  const AddSesion({Key? key}) : super(key: key);

  @override
  State<AddSesion> createState() => _AddSesionState();
}

class _AddSesionState extends State<AddSesion> {
  @override
  Widget build(BuildContext context) {
    TextEditingController _subjectController = TextEditingController();
    TextEditingController _locationControler = TextEditingController();

    Future addSessions() {
      final subject = _subjectController.text;
      final location = _locationControler.text;

      final refSession =
          FirebaseFirestore.instance.collection('sessions').doc();
      return refSession
          .set({
            'Subject Name': subject,
            'Location': location,
          })
          .then((value) => log("Colloction Added"))
          .catchError((onError) => log(onError));
    }

    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          child: TextField(
            controller: _subjectController,
            decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(12)),
                labelText: 'Enter Subject name'),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          child: TextField(
            controller: _locationControler,
            decoration: InputDecoration(
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(12)),
              labelText: 'Enter location',
            ),
          ),
        ),
        ElevatedButton(
            onPressed: () {
              AddSesion();
            },
            child: Text('Add Session')),
      ],
    );
  }
}
